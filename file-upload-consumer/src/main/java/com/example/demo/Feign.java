package com.example.demo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

@FeignClient(value = "fileUploadFeignClient",url = "http://localhost:8081",configuration = {FeignClientConfig.class})
public interface Feign {

	@PostMapping(value="/upload-file", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<Object> uploadFile(@PathVariable(value = "file") MultipartFile file);

	
	
}
