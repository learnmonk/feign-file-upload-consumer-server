package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class FileUploadController {

	@Autowired
	FileUploadService fileUploadService;
	
	@PostMapping("/upload-file")
	public ResponseEntity uploadFile(@RequestParam("file") MultipartFile file) {
		fileUploadService.uploadFile();
		return ResponseEntity.noContent().build();
	}
}
