package com.example.demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

@Service
public class FileUploadServiceImpl implements FileUploadService{

	@Autowired
	Feign feign;   
	
	@Autowired
	ResourceLoader resourceLoader;
	@Override
	public void uploadFile() {
		MultiValueMap<String, Object> multiValueMap = new LinkedMultiValueMap<>();
		Resource resource = resourceLoader.getResource("F:\\git\\feign-file-upload\\testfile.txt");
		try {
			String path = "F:\\git\\feign-file-upload\\testfile.txt";
			MultipartFile multipartFile = getMultipart(path);
			ResponseEntity response = feign.uploadFile(multipartFile);
			System.out.println(response);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	
	private MultipartFile getMultipart(String path) throws IOException {
		File file = new File(path);
		FileItem fileItem = new DiskFileItem("testfile.txt", Files.probeContentType(file.toPath()), false, file.getName(), (int) file.length(), file.getParentFile());

		try {
		    InputStream input = new FileInputStream(file);
		    OutputStream os = fileItem.getOutputStream();
		    IOUtils.copy(input, os);
		    // Or faster..
		    // IOUtils.copy(new FileInputStream(file), fileItem.getOutputStream());
		} catch (IOException ex) {
		    // do something.
		}

		MultipartFile multipartFile = new CommonsMultipartFile(fileItem);
		
		return multipartFile;
	}

}
